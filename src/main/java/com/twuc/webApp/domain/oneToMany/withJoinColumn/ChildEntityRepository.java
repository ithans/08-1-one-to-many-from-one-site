package com.twuc.webApp.domain.oneToMany.withJoinColumn;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChildEntityRepository extends JpaRepository<ChildEntity, Long> {
}
